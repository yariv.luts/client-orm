/**
 * @jest-environment node
 */

import { ModelInterface } from "./interfaces/model.interface";
import { Query, WhereFilterOp } from "./query";
import { Moment } from "moment";
import * as moment_ from "moment";
import { ClientOrmRepository } from './repository';
import { ClientOrmGateway } from './gateway/abstract.gateway';

const moment = moment_;

/**
 * Base model orm application
 */
export class BaseModel implements ModelInterface {

  public static CREATED_AT_FLAG: string = "created_at";
  public static UPDATED_AT_FLAG: string = "updated_at";

  id!: string | number;
  referencePath!: string;
  endpointUrl!: string;
  protected _scope = 'default';
  protected _referencePath!: string;
  protected _collectionName!: string;
  protected _columnId: string = 'id';
  protected isAutoTime!: boolean;
  created_at!: any;
  updated_at!: any;
  columnId!: string;
  protected unlistenFunc!: any;
  protected is_exist: boolean = false;
  pathId!: string;
  protected currentModel!: this & BaseModel;
  protected static aliasFieldsMapper: any = {};
  protected static textIndexingFields: any = {};
  protected static ignoreFields: any = [];
  protected static fields: any = {};
  protected static requiredFields: Array<string> = [];
  protected static internalFields: Array<string> = [];
  protected globalModel!: this;
  protected currentQuery!: any;
  protected data: any = {};
  protected currentQueryListener!: any;
  protected modelType!: any;


  constructor() {
    this.initProp();
  }

  /**
   * Init properties
   */
  initProp() {
    if (!this['storedFields']) {
      this['storedFields'] = [];
    }
    if (!this['fields']) {
      this['fields'] = {};
    }
    if (!this['requiredFields']) {
      this['requiredFields'] = [];
    }
    if (!this['aliasFieldsMapper']) {
      this['aliasFieldsMapper'] = [];
    }
  }

  /**
   * Get object id
   */
  getId(): any {
    return this.id;
  }

  convertToUpercase(string: string) {
    var result = '';
    var isNeedToSwitch = false;
    for (var i = 0; i < string.length; i++) {
      if (string.charAt(i) == '_') {
        isNeedToSwitch = true;
        continue;
      } else if (isNeedToSwitch) {
        result += string.charAt(i).toUpperCase();
        isNeedToSwitch = false;
      } else {
        result += string.charAt(i);
      }
    }
    return result;
  }

  /**
   * Get path id
   */
  getPathId() {
    return this.pathId;
  }

  /**
   * Init fields
   */
  initFields(): void { }

  /**
   * Init exist
   */
  isExist(): boolean {
    return this.is_exist;
  }

  /**
   * Get one relation
   * @param model 
   */
  async getOneRel<T>(model: { new(): T }): Promise<T & BaseModel> {
    var object: any = this.getModel(model);
    var that: any = this;
    return await object.load(that[object.getPathId()]);
  }

  /**
   * Get many relation
   */
  async getManyRel<T>(model: {
    new(): T;
  }): Promise<Array<T & BaseModel>> {
    var object: any = this.getModel(model);
    var that: any = this;
    return await object
      .where(object.getPathId(), "==", that[object.getPathId()])
      .get();
  }

  getModel<T>(model: { new(): T; }): T & BaseModel {
    var m: any | T = model;
    var object: any = new m();
    object.setModelType(model);
    object.currentModel = object;
    //object.initFields();

    return <T & BaseModel>object;
  }

  getRepository(): typeof ClientOrmRepository {
    return ClientOrmRepository;
  }

  getGateway(): ClientOrmGateway<this> {
    var clientOrmGatewayClass = ClientOrmRepository.getGlobalGateway(this['scope']);
    return new clientOrmGatewayClass<this>(this.endpointUrl, ClientOrmRepository.getGlobalHeaders());
  }

  getEndpointUrl() {
    var endpoint = ClientOrmRepository.getGlobalEndpoint(this['scope']);
  }


  getCurrentModel(): this {
    var object: any = this.getModel(this.getModelType());
    return object;
  }

  toString(): string {
    var res: any = Object.assign({}, this.getData());
    if (this.getId()) {
      res.id = this.getId();
    }
    return JSON.stringify(res);
  }

  /**
   * load from string
   * @return fields array
   */
  loadFromString(jsonString: string): this {
    var model: any = this;
    var params = JSON.parse(jsonString);
    this.createFromData(params, model);
    return model;
  }

  /**
   * Init object from string
   * @return fields array
   */
  initFromString(jsonString: string): this {
    var model: any = this.getCurrentModel();
    var params = JSON.parse(jsonString);
    this.createFromData(params, model);
    return model;
  }

  setModelType(model: any): this {
    this.modelType = model;
    return this;
  }

  getModelType() {
    return this.modelType ? this.modelType : this.constructor;
  }

  static where<T>(
    this: { new(): T },
    fieldPath: string,
    opStr: string,
    value: any
  ): Query<T> {
    var that: any = this;
    var query = that.query().where(fieldPath, opStr, value);
    return query;
  }

  where<T>(
    this: { new(): T },
    fieldPath: string,
    opStr: string,
    value: any
  ): Query<T> {
    var that: any = this;
    var query = that.query().where(fieldPath, opStr, value);
    return query;
  }

  setId(id: string | number) {
    this.id = id;
    return this;
  }

  async load(
    id: string | number,
    params: { [key: string]: string } = {}
  ): Promise<this> {
    var that: any = this;
    if (that.observeLoadBefore) {
      that.observeLoadBefore();
    }
    var res: any = null;
    this.setId(id);
    res = await this.getGateway().getOne(id, this.getModelType());
    if (res) {
      this.copy(res);
    }
    if (res && res.observeLoadAfter) {
      res.observeLoadAfter();
    }
    return this;
  }

  async remove() {
    return await this.getGateway().remove(this);
  }

  static async init<T>(this: { new(): T },
    id: string | number
  ): Promise<T | null> {
    var object: any = new this();
    var res: any;
    object.setId(id);
    res = await object.getGateway().getOne(id, this);
    return res;
  }

  static query<T>(this: { new(): T }): Query<T> {
    var query = new Query<T>();
    var object: any = new this();
    object.setModelType(this);
    query.init(object);
    return query;
  }


  query(): Query<this> {
    var query = new Query<this>();
    var that: any = this;
    var object: any = that.getCurrentModel();
    query.init(object);
    return query;
  }

  getCollectionName(): string {
    return this['collectionName'];
  }

  static getCollectionName(): string {
    var object: any = new this();
    object.setModelType(this);
    return object['collectionName'];
  }



  static async getAll<T>(this: { new(): T },
    whereArr?: any,
    orderBy?: {
      fieldPath: string;
      directionStr?: string;
    },
    limit?: number,
    params?: { [key: string]: string }
  ): Promise<Array<T>> {
    var object: any = new this();
    object.setModelType(this);
    var query = object.query();
    if (whereArr) {
      query.setWhere(whereArr);
    }
    if (limit) {
      query.limit(limit);
    }
    if (orderBy) {
      query.orderBy(orderBy.fieldPath, orderBy.directionStr);
    }
    var res: any = await query.get();
    return res;
  }

  static createFromData(data: Object, targetObject?: any) {
    var params: any = data;
    var object: any = targetObject
      ? targetObject : new this();
    object.setModelType(this);
    return object.createFromData(data, object);
  }

  createFromData(data: Object, targetObject?: this): this {
    var object: any = !targetObject
      ? this.getCurrentModel()
      : targetObject;
    if (data[this.columnId]) {
      this.is_exist = true;
      this.setId(data[this.columnId]);
    }
    if (!object.getModelType()) {
      object.setModelType(this.getModelType());
    }
    if (data[this.convertToUpercase(BaseModel.CREATED_AT_FLAG)]) {
      this.created_at = data[this.convertToUpercase(BaseModel.CREATED_AT_FLAG)];
      delete data[this.convertToUpercase(BaseModel.CREATED_AT_FLAG)];
    }
    if (data[this.convertToUpercase(BaseModel.UPDATED_AT_FLAG)]) {
      this.updated_at = data[this.convertToUpercase(BaseModel.UPDATED_AT_FLAG)];
      delete data[this.convertToUpercase(BaseModel.UPDATED_AT_FLAG)];
    }
    for (var key in data) {
      if (object.hasField(key)) {
        //console.log(object.getPropertyName(key), data[key])
        object[object.getPropertyName(key)] = data[key];
      }
    }
    return object;
  }

  getOriginName(key: string) {
    var that: any = this;
    for (var originKey in that.aliasFieldsMapper) {
      if (that.aliasFieldsMapper[originKey] == key) {
        return originKey;
      }
    }
    return null
  }

  initFromData(data: Object, targetObject?: this): this {
    return this.createFromData(data, this);
  }

  /**
   * Set document data directly
   * @param key
   * @param value
   */
  setParam(key: string, value: any): this {
    this[key] = value;
    this['storedFields'].push(key);
    return this;
  }

  /**
   * Get document data directly
   * @param key
   * @param value
   */
  getParam(key: string, defaultValue: any): any {
    return typeof this[key] !== 'undefined' ? this[key] : defaultValue
  }

  initAutoTime(): void {
    if (this.isAutoTime) {
      if (!this.created_at) {
        this[BaseModel.CREATED_AT_FLAG] = new Date().getTime();
        this.created_at = new Date().getTime();
      }
      this[BaseModel.UPDATED_AT_FLAG] = new Date().getTime();
      this['storedFields'].push(BaseModel.CREATED_AT_FLAG);
      this['storedFields'].push(BaseModel.UPDATED_AT_FLAG);
      this.updated_at = new Date().getTime();
    }
  }

  makeId(length: number) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  getCreatedAt(): Moment | null {
    return this.created_at ? moment.unix(this.created_at / 1000) : null;
  }

  getUpdatedAt(): Moment | null {
    return this.updated_at ? moment.unix(this.updated_at / 1000) : null;
  }

  async save(): Promise<this> {
    var that: any = this;
    if (that.observeSaveBefore) {
      that.observeSaveBefore();
    }
    if (!this.verifyRequiredFields()) {
      return this;
    }
    this.initAutoTime();
    if (this.getId()) {
      this.getGateway().update(this);
    } else {
      this.getGateway().create(this);
    }
    if (that.observeSaveAfter) {
      that.observeSaveAfter();
    }
    return this;
  }

  getReferencePath(): string {
    return this.referencePath;
  }

  static async find<T>(this: { new(): T }, fieldPath: string,
    opStr: WhereFilterOp,
    value: any): Promise<Array<T>> {
    var that: any = this;
    return await that.where(fieldPath, opStr, value).get();
  }

  static async findOne<T>(this: { new(): T }, fieldPath: string,
    opStr: WhereFilterOp,
    value: any): Promise<T | null> {
    var that: any = this;
    return await that.where(fieldPath, opStr, value).getOne();
  }

  getRequiredFields(): Array<string> {
    var that: any = this;
    return that.requiredFields ? that.requiredFields : [];
  }

  verifyRequiredFields(): boolean {
    var that: any = this;
    var fields = this.getRequiredFields();
    var result = true;
    for (var i = 0; fields.length > i; i++) {
      if (that[fields[i]] == null || typeof that[fields[i]] === undefined) {
        result = false;
        console.error(
          this.referencePath +
          "/:" +
          this.pathId +
          " - " +
          "Can't save " +
          fields[i] +
          " with null!"
        );
      }
    }
    return result;
  }

  getFieldName(key: string): string {
    return this['aliasFieldsMapper'] && this['aliasFieldsMapper'][key] ? this['aliasFieldsMapper'][key] : key;
  }

  getPropertyName(key: string): string {
    if (this['aliasFieldsMapper']) {
      for (var alias in this['aliasFieldsMapper']) {
        if (this['aliasFieldsMapper'][alias] == key) {
          return alias;
        }
      }
    }
    return key;
  }

  hasField(field: string) {
    var proprtyName = this.getPropertyName(field);
    return proprtyName != field || this['fields'][field];
  }


  copy(object: this) {
    for (var key in object.data) {
      var val = typeof object[key] !== 'undefined' ? object[key] : object.data[key];
      this[this.getPropertyName(key)] = val;
      this['data'][this.getFieldName(key)] = val;
    }
  }

  /**
   * Alias of getDocumentData
   */
  getData(): Object {
    var result = {};
    var data = this.data;
    // console.log('data -- ',data);
    for (var key in data) {
      if (!(this['ignoredFields'] && this['ignoredFields'].includes(key))) {
        result[key] = data[key];
      }
    }
    if (this.getId()) {
      result[this.columnId] = this.getId();
    }
    return result;
  }

}    