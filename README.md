Arbel Client Orm is an [ORM](https://en.wikipedia.org/wiki/Object-relational_mapping)
that can run in NodeJS, Browser, Cordova, PhoneGap, Ionic, React Native, NativeScript, Expo, and Electron platforms
and can be used with TypeScript and JavaScript (ES5, ES6, ES7, ES8).

Client Orm supports only Active Record pattern for now.

Some Arbel Client Orm features:

- supports ActiveRecord
- suppoert dynamic crud gateways
- support out of the box nestjs + typeorm backend

And more...

With Client Orm your models look like this:

```typescript
import { Field, BaseModel, Model } from "@arbel/client-orm";

@Model({
  name: "members"
})
export class Member extends BaseModel {
  @Field({
    is_required: true
  })
  public name!: string;

  @Field({
    is_required: true,
    field_name: "photo_url"
  })
  public photoUrl!: string;
}
```

And your domain logic looks like this:

```typescript
const member = new Member();
member.name = "Timber";
member.photoUrl = "https://www.example.com/image.png";
member.save();

//To access the data in hierarchy
//Get the website google from the database
const google = await Website.findOne("domain", "==", "www.google.com");

//Get the linkes under google website
const links = await google.getModel(Link).getAll();

//Get all members
const allMembers = await Member.getAll();

//Get all members with age > 3 and weight > 30
const list = await Member.query()
  .where("age", ">", "3")
  .where("weight", ">", "30")
  .get();

//Get all members with age > 3 or age < 3 limit 10
const list = await Member.query()
  .where("age", ">", "3")
  .orWhere("age", "<", "3")
  .like("name", "%Anv")
  .limit(10)
  .get();

//Get the member tom
const tom = await Member.findOne("firstName", "==", "Tom");
```

## Installation

1. Install the npm package:

   `npm install @arbel/client-orm --save`

##### TypeScript configuration

Also, make sure you are using TypeScript compiler version **3.3** or greater,
and you have enabled the following settings in `tsconfig.json`:

```json
"emitDecoratorMetadata": true,
"experimentalDecorators": true,
"strictPropertyInitialization" : false,
```

You may also need to enable `es6` in the `lib` section of compiler options, or install `es6-shim` from `@types`.

## Quick Start

1.Create global connection

```typescript
import { ClientOrmRepository, NestClientOrmGateway } from "@arbel/client-orm";

ClientOrmRepository.initGlobalEndpoint("http://localhost:2000/client-db/crud/");
```

2.Create global gateway

```typescript
ClientOrmRepository.initGlobalGateway(NestClientOrmGateway);
```

3.Create new object

```typescript
import { Member } from "model/member";

const member = new Member();
member.name = "Timber";
member.photoUrl = "https://www.example.com/image.png";
member.save();
```
