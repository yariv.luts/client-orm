import { FieldOptions } from "../interfaces/field.options.interface";

export function Field(options?: FieldOptions): any {
  return (target: any, key: string) => {
    let val: any;
    if (!target.requiredFields) {
      target.requiredFields = new Array();
    }

    if (!target.aliasFieldsMapper) {
      target.aliasFieldsMapper = {};
    }

    if (!target.fields) {
      target.fields = {};
    }
    if (!target.ignoredFields) {
      target.ignoredFields = [];
    }
    if (!target.internalFields) {
      target.internalFields = [];
    }
    if (options && options.field_name) {
      target.aliasFieldsMapper[key] = options.field_name;
    }


    target.fields[key] = true;

    if (options && options.is_required) {
      target.requiredFields.push(key);
    }
    var field_name =
      options && options.field_name ? options.field_name : key;

    if (!target.storedFields) {
      target.storedFields = [];
    }
    target.storedFields.push(field_name);

    var update = Object.defineProperty(target, key, {
      configurable: true,
      enumerable: true,
      /*  writable: true, */
      set: function (value) {
        this['data'][this.getFieldName(key)] = value;
      },
      get: function () {
        return typeof this['data'][this.getFieldName(key)] === undefined ? undefined : this['data'][this.getFieldName(key)];
      },
    });
    // If the update failed, something went wrong
    if (!update) {
      //Kill everything
      throw new Error("Unable to update property");
    }

    return target;
  };
}
