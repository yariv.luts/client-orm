import { ClientOrmGateway } from './gateway/abstract.gateway';

export class ClientOrmRepository {

    static endpoints = {};
    static globalGateways = {};
    static globalHeaders = {};

    static initGlobalEndpoint(endpointUrl: string, scope: string = 'default') {
        this.endpoints[scope] = endpointUrl;
    }

    static initGlobalGateway(typeClientOrmGateway: typeof ClientOrmGateway, scope: string = 'default') {
        this.globalGateways[scope] = typeClientOrmGateway;
    }

    static initGlobalHeaders(headers: any, scope: string = 'default') {
        this.globalHeaders[scope] = headers;
    }

    static getGlobalHeaders(scope: string = 'default') {
        if (!this.globalHeaders[scope]) {
            return {};
        } else {
            return this.globalHeaders[scope];
        }
    }

    static getGlobalGateway(scope: string = 'default') {
        if (!this.globalGateways[scope]) {
            throw "Missing global client orm gateway " + scope;
        } else {
            return this.globalGateways[scope];
        }
    }

    static getGlobalEndpoint(scope: string = 'default') {
        if (!this.endpoints[scope]) {
            throw "Missing global client orm gateway " + scope;
        } else {
            return this.endpoints[scope];
        }
    }


}