export interface ModelOptions {

  /**
   * Endpoint url (if global endpoint is not defined)
   */
  endpoint_url?: string;

  /**
   * Table Name / Collection Name
   */
  name?: string;

  /**
   * Repository Scope
   */
  scope?: string;

  /**
   * The name of the id column 
   */
  column_id?: string;

  /**
   * Is generate local user time
   */
  auto_time?: boolean
}