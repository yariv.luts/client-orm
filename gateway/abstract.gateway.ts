import { BaseModel } from 'orm/base.model';

export abstract class ClientOrmGateway<T> {


    constructor(protected url: string, protected headers: any = {}) {
    }

    abstract getAll(params: any, model: typeof BaseModel): Promise<{
        result: Array<BaseModel & T>,
        count: number
    }>;

    async abstract getOne(id: string | number, model: typeof BaseModel): Promise<BaseModel & T>;

    async abstract update(model: BaseModel): Promise<BaseModel & T>;

    async abstract remove(model: BaseModel): Promise<any>;

    async abstract create(model: BaseModel): Promise<BaseModel & T>;

    initObjectFromData(data, model: typeof BaseModel & T): BaseModel & T {
        for (var key in data) {
            var object: any = new model();
            object.init
            /* if (object.hasField(key)) {
                object[object.getPropertyName(key)] = data[key];
            } */
        }
        return object;
    }

    initObjectsFromDataList(data: any[], model: typeof BaseModel & T): Array<BaseModel & T> {
        var result: Array<BaseModel & T> = [];
        data.forEach((row) => {
            var object = model.createFromData(row);
            result.push(object);
        })
        return result;
    }



}