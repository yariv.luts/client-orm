
import { BaseModel } from '../base.model';
import * as axios from 'axios';
import { ClientOrmGateway } from './abstract.gateway';

export class NestClientOrmGateway<T> extends ClientOrmGateway<T> {

    protected http = axios.default;

    constructor(url: string, headers: any = {}) {
        super(url, headers);
        this.initCommonHeaders(headers);
    }

    serialize(obj, prefix) {
        var str = [],
            p;
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                var k = prefix ? prefix + "[" + p + "]" : p,
                    v = obj[p];
                str.push((v !== null && typeof v === "object") ?
                    this.serialize(v, k) :
                    encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
        }
        return str.join("&");
    }

    getUrl(modelName: string, id?: string | number, params?: any) {
        return this.url + modelName + (id ? '/' + id : '') + (params ? '?params=' + encodeURI(JSON.stringify(params)) : '');
    }

    async getAll(params = {}, model: typeof BaseModel & T): Promise<{
        result: Array<BaseModel & T>,
        count: number
    }> {
        var res: any = await this.http.get(this.getUrl(model.getCollectionName(), null, params));
        return {
            result: this.initObjectsFromDataList(res.data.result, model),
            count: res.data.count
        };
    }


    async getOne(id: string | number, model: typeof BaseModel & T) {
        var res = await this.http.get(this.getUrl(model.getCollectionName(), id));
        return model.createFromData(res.data.result);
    }

    async update(model: BaseModel & T) {
        var res = await this.http.put(this.getUrl(model.getCollectionName(), model.id), model.getData());
        return model.createFromData(res.data.result);
    }

    async remove(model: BaseModel & T) {
        var res = await this.http.delete(this.getUrl(model.getCollectionName(), model.id));
        return res.data;
    }

    async create(model: BaseModel & T) {
        var res = await this.http.post(this.getUrl(model.getCollectionName(), model.id), model.getData());
        return model.createFromData(res.data.result);
    }

    initCommonHeaders(headers: any) {
        for (var key in headers) {
            this.http.defaults.headers.common[key] = headers[key];
        }
    }
}