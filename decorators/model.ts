import { ModelOptions } from "../interfaces/model.options.interface";

import { BaseModel } from "../base.model";
import { ClientOrmRepository } from '../repository';

function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      var t: any = Object.getOwnPropertyDescriptor(baseCtor.prototype, name);
      Object.defineProperty(derivedCtor.prototype, name, t);
    });
  });
}

export function Model(options: ModelOptions) {
  return function <T extends { new(...args: any[]): {} }>(constructor: T) {

    if (Object.getPrototypeOf(constructor) !== BaseModel) {
      applyMixins(constructor, [BaseModel]);
    }
    //console.log('constructor.prototype ', constructor.prototype, constructor);

    Object.defineProperty(constructor.prototype, 'endpointUrl', {
      get: function () {
        return this._endpointUrl ? this._endpointUrl : (
          options.endpoint_url ? options.endpoint_url : ClientOrmRepository.getGlobalEndpoint(this.scope)
        );
      },
      set: function (value) {
        this._endpointUrl = value;
      }
    });




    Object.defineProperty(constructor.prototype, 'collectionName', {
      get: function () {
        return this._collectionName ? this._collectionName : options.name;
      },
      set: function (value) {
        this._collectionName = value;
      }
    });

    Object.defineProperty(constructor.prototype, 'scope', {
      get: function () {
        return options.scope ? options.scope : this._scope;
      },
      set: function (value) {
        this._scope = value;
      }
    });

    Object.defineProperty(constructor.prototype, 'isAutoTime', {
      get: function () {
        return typeof options.auto_time === "undefined" ? true : options.auto_time;
      },
      set: function (value) {

      }
    });

    Object.defineProperty(constructor.prototype, 'columnId', {
      get: function () {
        return typeof this._columnId ? this._columnId : options.column_id;
      },
      set: function (value) {
        this._columnId = value;
      }
    });
    return constructor;
  }
}
