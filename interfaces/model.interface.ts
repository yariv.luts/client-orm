
import { Moment } from 'moment';
import { BaseModel } from "../base.model";

/**
 * Client Orm
 * Model interface 
 */
export interface ModelInterface {

  /**
   * Get Id
   * @return string
   */
  getId(): string | number;

  /**
   * Set Id
   * @param id string
   */
  setId(id: string | number): this;


  /**
   * Check if object exist inside the db
   */
  isExist(): boolean;

  /**
   * Verfiy required fields 
   * @return boolean
   */
  verifyRequiredFields(): boolean;


  /**
   * Get Id
   * @return string
   */
  getCreatedAt(): Moment | null;

  /**
   * Get Id
   * @return string
   */
  getUpdatedAt(): Moment | null;


  /**
   * Get Id
   * @return string
   */
  getId(): string;

  /**
   * Get current model
   */
  getCurrentModel(): this;


  /**
   * Load model by id
   * @param id - string
   * @param params - path paremeters
   */
  load(
    id: string,
    params?: { [key: string]: string }
  ): Promise<this>;

  /**
   * Set document data directly
   * @param key 
   * @param value 
   */
  setParam(key: string, value: any): this;

  /**
   * Set document data directly
   * @param key 
   * @param value 
   */
  getParam(key: string, defaultValue: any): any;

  /**
   * Remove the current model
   */
  remove(): Promise<boolean>;

  /**
   * Get relation one
   * @param model 
   */
  getOneRel<T>(model: { new(): T }): Promise<T & BaseModel>;

  /**
   * Get relation many
   * @param model 
   */
  getManyRel<T>(model: { new(): T }): Promise<Array<T & BaseModel>>;

  /**
   * Init the current model by firestore object
   * @param doc 
   * @return Current model
   */
  initFromData(doc: any): this;

  /**
   * Factory method of model object
   * @param model - The class reference
   * @return initialize model object
   */
  getModel<T>(model: { new(): T }): T & BaseModel;

  /**
   * Get required fields
   * @return fields array 
   */
  getRequiredFields(): Array<string>;

  /**
   * Get data in json string
   * @return string
   */
  toString(): string;

  /**
   * load from string
   * @return this
   */
  loadFromString(jsonString: string): this;

  /**
   * Init object from string
   * @return new model
   */
  initFromString(jsonString: string): this;

  /**
   * Save action
   * Store the document data inside firestore
   */
  save(): Promise<this>;
}
