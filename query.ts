import { BaseModel } from "./base.model";

/**
 * The direction of a `Query.orderBy()` clause is specified as 'desc' or 'asc'
 * (descending or ascending).
 */
export type OrderByDirection = 'desc' | 'asc';

/**
 * Filter conditions in a `Query.where()` clause are specified using the
 * strings '<', '<=', '==', '>=', '>', and 'array-contains'.
 */
export type WhereFilterOp = '<' | '<=' | '==' | '>=' | '>' | 'in' | '!=' | 'is null' | 'raw' | 'like';

/**
 * {
    select: ["firstName", "lastName"],
    relations: ["profile", "photos", "videos"],
    where: {
        firstName : {
          op : '==',
          val : 'Timber'
        },
        lastName : {
          op : '==',
          val : 'Saw'
        },
    },
    order: {
        name: "ASC",
        id: "DESC"
    },
    skip: 5,
    take: 10,
    cache: true
}
 */
export class Query<T> {
  protected model!: BaseModel;
  protected data = {
    relations: [],
    where: [],
    order: {
    },
    cache: false
  };
  protected queryResult!: number;
  protected total: number = null;
  protected currentWherePos = 0;
  init(model: BaseModel) {
    this.model = model;
  }

  /*  get current(){
     if(!this._current){
       return this.currentRef;
     }
     return this._current;
   }
 
   set current(value){
     this._current = value;
   } */

  /**
   * Creates and returns a new Query with the additional filter that documents
   * must contain the specified field and the value should satisfy the
   * relation constraint provided.
   *
   * @param fieldPath The path to compare
   * @param opStr The operation string (e.g "<", "<=", "==", ">", ">=", "!=").
   * @param value The value for comparison
   * @return The created Query.
   */
  where(
    fieldPath: string,
    opStr: WhereFilterOp,
    value: any
  ): Query<T> {
    var field: any = fieldPath;
    fieldPath = this.model.getFieldName(field);
    if (!this.data.where[this.currentWherePos]) {
      this.data.where[this.currentWherePos] = {};
    }
    this.data.where[this.currentWherePos][fieldPath] = {
      op: opStr,
      value: value
    }
    return this;
  }

  getTotal() {
    return this.total;
  }

  /**
   * Creates and returns a new Query with the additional filter that documents
   * must contain the specified field and the value should satisfy the
   * relation constraint provided.
   *
   * @param fieldPath The path to compare
   * @param opStr The operation string (e.g "<", "<=", "==", ">", ">=", "!=").
   * @param value The value for comparison
   * @return The created Query.
   */
  orWhere(
    fieldPath: string,
    opStr: WhereFilterOp,
    value: any
  ): Query<T> {
    this.currentWherePos++;
    return this.where(fieldPath, opStr, value);
  }

  /**
   * Creates and returns a new Query that's additionally sorted by the
   * specified field, optionally in descending order instead of ascending.
   *
   * @param fieldPath The field to sort by.
   * @param directionStr Optional direction to sort by (`asc` or `desc`). If
   * not specified, order will be ascending.
   * @return The created Query.
   */
  orderBy(
    fieldPath: string,
    directionStr?: OrderByDirection
  ): Query<T> {
    this.data.order[fieldPath] = directionStr;
    return this;
  }

  /**
   * Creates and returns a new Query where the results are limited to the
   * specified number of documents.
   *
   * @param limit The maximum number of items to return.
   * @return The created Query.
   */
  limit(limit: number): Query<T> {
    this.data['take'] = limit;
    return this;
  }

  /**
   * Creates and returns a new Query where the results are limited to the
   * specified number of documents.
   *
   * @param limit The maximum number of items to return.
   * @return The created Query.
   */
  offset(offset: number): Query<T> {
    this.data['skip'] = offset;
    return this;
  }

  like(fieldName: string, find: string): Query<T> {
    this.where(fieldName, 'like', find);
    return this;
  }

  getData() {
    var result = {};
    for (var key in this.data) {
      if ((Array.isArray(this.data[key]) && this.data[key].length > 0) ||
        Object.keys(this.data[key]).length > 0) {
        result[key] = this.data[key];
      }
    }
    console.log(this.data, result);
    return this.data;
  }

  /**
   * Executes the query and returns the results as a `QuerySnapshot`.
   *
   * Note: By default, get() attempts to provide up-to-date data when possible
   * by waiting for data from the server, but it may return cached data or fail
   * if you are offline and the server cannot be reached. This behavior can be
   * altered via the `GetOptions` parameter.
   *
   * @param options An object to configure the get behavior.
   * @return A Promise that will be resolved with the results of the Query.
   */
  async get(
  ): Promise<Array<BaseModel & T>> {
    var res: any = await this.model.getGateway().getAll(this.getData(), this.model.getModelType());
    this.total = res.count;
    return res.result;
  }

  /**
   * Executes the query and returns the results as a `QuerySnapshot`.
   *
   * Note: By default, get() attempts to provide up-to-date data when possible
   * by waiting for data from the server, but it may return cached data or fail
   * if you are offline and the server cannot be reached. This behavior can be
   * altered via the `GetOptions` parameter.
   *
   * @param options An object to configure the get behavior.
   * @return A Promise that will be resolved with the results of the Query.
   */
  async getOne(): Promise<BaseModel & T | null> {
    this.limit(1);
    var list = await this.get();
    return list.length > 0 ? list[0] : null;
  }

  setWhere(whereData: any) {
    this.data.where = whereData;
  }

}
